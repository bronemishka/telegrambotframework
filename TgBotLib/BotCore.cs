﻿using System.Reflection;
using Config.Net;
using Microsoft.Extensions.DependencyInjection;
using ModuleBase.Command;
using Serilog;
using Telegram.Bot;
using Telegram.Bot.Polling;
using Telegram.Bot.Types;
using TgBotLib.Model;
using TgBotLib.Utils;
using User = Telegram.Bot.Types.User;

namespace TgBotLib
{
    public class BotCore
    {
        private IMainSettings? Settings { get; set; }

        private TelegramBotClient? BotClient { get; set; }
        private User? BotInfo { get; set; }

        public CancellationTokenSource BotCancellationToken = new();

        public IServiceProvider? ServiceProvider { get; set; }

        /// <summary>
        /// Stopping bot instance
        /// !!!Do this, before reinitializing bot instance!!!
        /// </summary>
        public async Task StopBotInstance()
        {
            try
            {
                if (BotClient == null) return;
                await BotCancellationToken.CancelAsync();
                BotCancellationToken.Dispose();
                await BotClient.DeleteWebhook();
                await BotClient.Close();
                Log.Information("Stopped bot instance");
            }
            catch (Exception e)
            {
                Log.Error("Exception on stopping bot instance\n>>>{Message}", e.Message);
            }
        }

        /// <summary>
        /// Initializes bot instance
        /// </summary>
        public async Task InitBot()
        {
            Log.Information("Initializing bot instance");
            
            Settings = new ConfigurationBuilder<IMainSettings>()
                .UseJsonFile("config.json")
                .Build();
            var serviceCollection = new ServiceCollection();
            
            try
            {
                Log.Information("Connecting to Telegram API...");

                BotClient = new TelegramBotClient(Settings.BotToken);

                serviceCollection.AddSingleton<ITelegramBotClient>(BotClient);

                BotCancellationToken = new CancellationTokenSource(); // Updating Cancellation token source
                BotInfo = await BotClient.GetMe();

                Log.Information("Connected to the {FirstName}(@{Username})",
                    BotInfo.FirstName,
                    BotInfo.Username);

                serviceCollection.InitModules(BotClient);

                serviceCollection.RegisterAllCommands(Assembly.GetExecutingAssembly());

                ServiceProvider = serviceCollection.BuildServiceProvider();

                BotClient.StartReceiving(HandlingModule.HandleUpdate, HandlingModule.HandleUpdateError,
                    new ReceiverOptions { DropPendingUpdates = true }, BotCancellationToken.Token);
                
                await BotClient.SetMyCommands(ServiceProvider.GetServices<IBotCommand>()
                    .Where(x => !x.HiddenCommand).Select(x =>
                        new BotCommand { Command = x.Name, Description = x.Description ?? string.Empty }));

                HandlingModule.CommandService = ServiceProvider;
            }
            catch (ArgumentException)
            {
                Settings.BotToken = "FILL_ME_WITH_TOKEN";
                Log.Warning("Bot token is empty or invalid, reinitialize instance after providing with working one");
            }
            catch (Exception e)
            {
                Log.Error("Critical error caught!\n{error}\n{stack}\n{info}", e.Message, e.StackTrace,
                    e.InnerException?.Message);
            }
        }
    }
}