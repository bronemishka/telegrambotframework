﻿using Config.Net;

namespace TgBotLib.Model
{
    public interface IMainSettings
    {
        [Option(DefaultValue = "")]
        public string BotToken { get; set; }
    }
}