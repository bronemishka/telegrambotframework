﻿using ModuleBase.Command;
using TgBotLib.Database;
using User = TgBotLib.Permissions.Model.User;

namespace TgBotLib.Permissions;

public static class Core
{
    public static void UpdateOrRegisterUser(Telegram.Bot.Types.User? userInfo)
    {
        if (userInfo == null) return;
        using var dbQuery = new Database.Query();
        var user = dbQuery.All<User>().FirstOrDefault(x => x.UserId == userInfo.Id);
        
        if (user == null)
        {
            dbQuery.Add(new User {UserId = userInfo.Id, Username = userInfo.Username ?? string.Empty});
            return;
        }

        if (userInfo.Username == user.Username) return;
        
        user.Username = userInfo.Username ?? string.Empty;
        dbQuery.Update(user);
    }
    
    public static bool HaveAccess(long id, IBotCommand command)
    {
        using var dbQuery = new Query();
        var user = dbQuery.All<User>().FirstOrDefault(x => x.UserId == id);

        if (user == null) return false;
        
        if (user.IsSuperAdmin)
        {
            return true;
        }

        List<string> allowedCommands = [];

        var lastGroup = user.Group;

        do
        {
            if (lastGroup == null) break;
            allowedCommands.AddRange(user.Group!.AllowedCommands!);
            lastGroup = lastGroup.Parent;
        } while (lastGroup != null);

        return allowedCommands.Contains(command.Name);
    }
}