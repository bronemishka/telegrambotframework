﻿using System.ComponentModel.DataAnnotations;
using TgBotLib.Database;

namespace TgBotLib.Permissions.Model;

public class User : BaseEntity
{
    [Required] public string Username { get; set; }
    [Required] public long UserId { get; set; }

    public UserGroup? Group { get; set; }

    public bool IsSuperAdmin { get; set; }
}