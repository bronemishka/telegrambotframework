﻿using System.ComponentModel.DataAnnotations;
using TgBotLib.Database;

namespace TgBotLib.Permissions.Model;

public class UserGroup : BaseEntity
{
    public UserGroup? Parent { get; set; }

    public List<string>? AllowedCommands { get; set; }

    [Required] public string Name { get; set; }
}