﻿using Microsoft.EntityFrameworkCore;

namespace TgBotLib.Database
{
    [PrimaryKey(nameof(Id))]
    public abstract class BaseEntity
    {
        public Guid Id { get; init; }

        public DateTime Created { get; init; } = DateTime.Now;

        public DateTime? Modified { get; set; }

        public bool Deleted { get; set; }

    }
}
