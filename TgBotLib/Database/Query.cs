﻿using Serilog;

namespace TgBotLib.Database
{
    public sealed class Query : IDisposable
    {
        private readonly AppDbContext _context = new();

        public IQueryable<T> All<T>() where T : BaseEntity
        {
            return _context.Set<T>()
                .Where(x => !x.Deleted);
        }

        public void Update<TEntity>(TEntity entityToUpdate) where TEntity : BaseEntity
        {
            entityToUpdate.Modified = DateTime.Now;
            _context.Update(entityToUpdate);
            Log.Information("Entity updated ({0})", entityToUpdate.Id);
        }

        public void Update<TEntity>(IEnumerable<TEntity> entitiesToUpdate) where TEntity : BaseEntity
        {
            foreach (var entity in entitiesToUpdate)
            {
                Update(entity);
            }
        }

        public void Add<TEntity>(TEntity entityToAdd) where TEntity : BaseEntity
        {
            _context.Add(entityToAdd);
            Log.Information("New entity registered ({0})", entityToAdd.Id);
        }

        public void Remove<TEntity>(TEntity entityToRemove) where TEntity : BaseEntity
        {
            var update = _context.Update(entityToRemove);
            update.Property(x => x.Deleted).CurrentValue = true;
            Log.Information("Entity marked to remove ({0})", entityToRemove.Id);
        }

        public void Dispose()
        {
            _context.SaveChanges();
            _context.Dispose();
        }
    }
}