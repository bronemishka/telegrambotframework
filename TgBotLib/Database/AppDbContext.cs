﻿using Microsoft.EntityFrameworkCore;

namespace TgBotLib.Database
{
    public sealed class AppDbContext : DbContext
    {
        public AppDbContext()
        {
            Database.EnsureCreated();
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlite("Data Source=db.sqlite");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            var type = typeof(BaseEntity);

            var derivedTypes = type.Assembly.GetTypes()
                .Where(x => x is { IsClass: true, IsAbstract: false } && type.IsAssignableFrom(x));

            foreach(var derivedType in derivedTypes)
            {
                modelBuilder.Entity(derivedType).ToTable($"{derivedType.Name}");
            }
        }
    }
}
