using ModuleBase.Command;
using Serilog;
using TgBotLib.Database;
using TgBotLib.Permissions.Model;

namespace TgBotLib.Commands;

public class ActivateAdminCommand : IBotCommand
{
    private string? _adminId;

    public ActivateAdminCommand()
    {
        Action = ActivateAdmin;
        
        //Creating guid-code to become an admin
        if (new Query().All<User>().Any(x => x.IsSuperAdmin))
        {
            HiddenCommand = true;
            return;
        }
        
        _adminId = Guid.NewGuid().ToString();
        Log.Warning("No admins found, use {command} {code} to become the first one", $"/{Name}",
            _adminId);
    }

    public bool HiddenCommand { get; set; }
    public string Name { get; set; } = "admin";
    public string? Description { get; set; } = "Allows to become an admin";
    public bool FreeAccess { get; set; } = true;
    public Func<CommandParam, Task<bool>> Action { get; set; }

    private Task<bool> ActivateAdmin(CommandParam param)
    {
        if (_adminId == null)
        {
            return Task.FromResult(false);
        }

        var input = param.Args?.FirstOrDefault();

        if (!Guid.TryParse(input, out var id)) return Task.FromResult(false);

        if (id.ToString() != _adminId) return Task.FromResult(false);

        using (var dbQuery = new Query())
        {
            var user = dbQuery.All<Permissions.Model.User>().FirstOrDefault(x => x.UserId == param.Caller);

            if (user == null) return Task.FromResult(false);

            user.IsSuperAdmin = true;
        }

        Log.Information("{userId} is now SuperAdmin!", param.Caller);
        _adminId = null;
        return Task.FromResult(true);
    }
}