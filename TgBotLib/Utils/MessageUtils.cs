﻿using Serilog;
using Telegram.Bot;
using Telegram.Bot.Types;
using Telegram.Bot.Types.ReplyMarkups;

namespace TgBotLib.Utils
{
    public static class MessageUtils
    {
        private static string? GetChatTitle(this Message msg) => msg.Chat.Title ?? msg.Chat.Username;
        
        public static async Task<Message?> SendMessage(this ITelegramBotClient? client, ChatId chatId, MessageWrap wrap)
        {
            if (client == null) return null;
            try
            {
                Task<Message> msgInfoAwaiter;
                ReplyParameters? replyParams = null;
                if (wrap.Image == null || wrap.Image.Length == 0)
                {
                    
                    if (wrap.ReplyMessageId != null)
                    {
                        replyParams = new ReplyParameters { MessageId = (int)wrap.ReplyMessageId };
                    }
                    
                    msgInfoAwaiter = client.SendMessage(chatId, wrap.Text, disableNotification: wrap.DisableNotify, replyMarkup: wrap.Keyboard, replyParameters: replyParams );
                }
                else
                {
                    msgInfoAwaiter = client.SendPhoto(chatId, new InputFileStream(wrap.Image), caption: wrap.Text, disableNotification: wrap.DisableNotify, replyMarkup: wrap.Keyboard);
                }
                await msgInfoAwaiter.WaitAsync(TimeSpan.FromSeconds(5));
                Log.Information("Created message({MessageId}) in ({ChatTitle} | {Id})", msgInfoAwaiter.Result.MessageId, msgInfoAwaiter.Result.GetChatTitle(), msgInfoAwaiter.Result.Chat.Id);
                return msgInfoAwaiter.Result;
            }
            catch (Exception e)
            {
                Log.Error("Unable to send message in {Chat}\n>>>{Exception}", chatId, e.Message);
            }
            return null;
        }

        public static async Task EditMessage(this ITelegramBotClient? client, ChatId chatId, MessageId msgToEdit,
            MessageWrap wrap)
        {
            if (client == null) return;
            try
            {
                Task<Message> msgInfoAwaiter;
                if (wrap.Image == null || wrap.Image.Length == 0)
                {
                    msgInfoAwaiter = client.EditMessageText(chatId, msgToEdit.Id, wrap.Text, replyMarkup: wrap.Keyboard);
                }
                else
                {
                    var media = new InputMediaPhoto(new InputFileStream(wrap.Image, "art")) { Caption = wrap.Text };
                    msgInfoAwaiter = client.EditMessageMedia(chatId, msgToEdit.Id, media, replyMarkup: wrap.Keyboard);
                }
                await msgInfoAwaiter.WaitAsync(TimeSpan.FromSeconds(5));
                Log.Information("Edited message({MessageId}) in ({ChatTitle} | {Id})", msgInfoAwaiter.Result.MessageId, msgInfoAwaiter.Result.GetChatTitle(), msgInfoAwaiter.Result.Chat.Id);
            }
            catch (Exception e)
            {
                Log.Error("Unable to edit message in {Chat}\n>>>{Exception}", chatId, e.Message);
            }
        }

        public static async Task DeleteMessage(this ITelegramBotClient? client, ChatId chatId, MessageId messageId)
        {
            try
            {
                if (client != null) await client.DeleteMessage(chatId, messageId.Id, CancellationToken.None);
                Log.Debug("Message {Id} removed from {Chat}", messageId.Id, chatId);
            }
            catch (Exception e)
            {
                Log.Error("Cant delete message {Id} from {Chat}\n>>>{Message}", messageId.Id, chatId, e.Message);
            }
        }
        
        public class MessageWrap
        {
            public string Text = string.Empty;
            public bool DisableNotify;
            public MemoryStream? Image = null;
            public InlineKeyboardMarkup? Keyboard = null;
            public int? ReplyMessageId;
        }
    }
}