﻿using System.Reflection;
using Microsoft.Extensions.DependencyInjection;
using ModuleBase;
using Serilog;
using Telegram.Bot;

namespace TgBotLib.Utils;

public static class ModulesCore
{
    private static readonly string ModulesPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Modules");

    public static void InitModules(this IServiceCollection serviceCollection, TelegramBotClient client)
    {
        Log.Information("Loading modules");

        if (!Directory.Exists("Modules"))
        {
            Directory.CreateDirectory("Modules");
        }

        var files = Directory.EnumerateFiles(ModulesPath).Where(x => x.EndsWith(".dll")).ToList();

        Log.Information("Found {num} files", files.Count);

        foreach (var module in files)
        {
            Log.Information("Getting modules from {module}", module);

            try
            {
                var assembly = Assembly.Load(File.ReadAllBytes(Path.Combine(ModulesPath, module)));
                
                serviceCollection.RegisterAllCommands(assembly);
                
                var loaded = assembly.GetExportedTypes()
                    .Where(type => typeof(IBotModule).IsAssignableFrom(type) && !type.IsInterface)
                    .Select(x => (IBotModule)Activator.CreateInstance(x)!).ToList();

                loaded.ForEach(x =>
                {
                    serviceCollection.AddSingleton(x);
                    Log.Information("Loaded {module} ({version})", x.ModuleName, x.ModuleVersion);
                });

                foreach (var moduleInstance in loaded)
                {
                    moduleInstance.LoadModule(client);
                }
            }
            catch (Exception e)
            {
                throw new Exception($"Unable to load {module}", e);
            }
        }
        
        Log.Information("Loaded and initialized {cnt} modules", serviceCollection.Count(x => x.ServiceType == typeof(IBotModule)));
    }
}