﻿using Microsoft.Extensions.DependencyInjection;
using ModuleBase.Command;
using Serilog;
using Telegram.Bot;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;

namespace TgBotLib.Utils;

public static class HandlingModule
{
    public static event Action<Message> MessageHandled;

    // TODO: Rework this?
    public static IServiceProvider? CommandService { get; set; }

    public static async Task HandleUpdate(this ITelegramBotClient client, Update update, CancellationToken ct)
    {
        await Task.Run(() =>
        {
            Permissions.Core.UpdateOrRegisterUser(update.Message?.From);

            Log.Debug("Received update {update}", update.Type);
            switch (update.Type)
            {
                case UpdateType.Message:
                {
                    var message = update.Message;

                    if (message is null)
                    {
                        return;
                    }

                    if (message.Text != null && message.Text.StartsWith('/'))
                    {
                        // At this moment we suggest that sent message is command
                        HandleCommand(message);
                    }

                    MessageHandled?.Invoke(message);
                    break;
                }
                case UpdateType.Unknown:
                    Log.Debug("Unknown update handled");
                    break;
                case UpdateType.InlineQuery:
                case UpdateType.ChosenInlineResult:
                case UpdateType.CallbackQuery:
                case UpdateType.EditedMessage:
                case UpdateType.ChannelPost:
                case UpdateType.EditedChannelPost:
                case UpdateType.ShippingQuery:
                case UpdateType.PreCheckoutQuery:
                case UpdateType.Poll:
                case UpdateType.PollAnswer:
                case UpdateType.MyChatMember:
                case UpdateType.ChatMember:
                case UpdateType.ChatJoinRequest:
                case UpdateType.MessageReaction:
                case UpdateType.MessageReactionCount:
                case UpdateType.ChatBoost:
                case UpdateType.RemovedChatBoost:
                case UpdateType.BusinessConnection:
                case UpdateType.BusinessMessage:
                case UpdateType.EditedBusinessMessage:
                case UpdateType.DeletedBusinessMessages:
                case UpdateType.PurchasedPaidMedia:
                default:
                    Log.Debug("Unimplemented update handled ({type})", update.Type);
                    break;
            }
        }, ct);
    }

    private static void HandleCommand(Message message)
    {
        var cmd = message.Text?[1..]?.Split(' ')[0];
        if (cmd is null or "")
        {
            return;
        }

        var cmdParam = new CommandParam
        {
            BotClient = CommandService?.GetService<ITelegramBotClient>(),
            Args = message.Text?[1..]?.Split(' ').Skip(1).ToList(),
            Caller = message.From!.Id,
            ChatId = message.Chat.Id
        };
        if (message.From.Username != null && cmd.Contains('@'))
        {
            if (cmd.EndsWith(message.From.Username))
            {
                cmd = cmd.Split('@')[0];
            }
            else
            {
                return; //If command is not related to our bot, then just do nothing
            }
        }


        var command = CommandService?.GetServices<IBotCommand>().FirstOrDefault(c => c.Name == cmd);

        if (command is null)
        {
            return;
        }

        if (command.FreeAccess || Permissions.Core.HaveAccess(message.From.Id, command))
        {
            Log.Information("{user} ran command {cmd}", cmdParam.Caller, cmd);
            command.Action.Invoke(cmdParam);
        }
        else
        {
            Log.Information("{user} tried to call command {command}", cmdParam.Caller, cmd);
        }
    }

    public static async Task HandleUpdateError(this ITelegramBotClient client, Exception exception,
        CancellationToken ct) =>
        await Task.Run(
            () =>
            {
                Log.Error("Error occured on handling update\n>>>{Message}\n{inner}", exception.Message,
                    exception.InnerException?.Message);
            },
            ct);
}