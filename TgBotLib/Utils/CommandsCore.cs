using System.Reflection;
using Microsoft.Extensions.DependencyInjection;
using ModuleBase.Command;
using Serilog;

namespace TgBotLib.Utils;

public static class CommandsCore
{
    public static void RegisterAllCommands(this IServiceCollection serviceCollection, Assembly assembly)
    {
        var commandsList = assembly
            .GetTypes()
            .Where(type => typeof(IBotCommand).IsAssignableFrom(type) && type is { IsClass: true, IsAbstract: false })
            .ToList();
        
        foreach (var type in commandsList)
        {
            Log.Information("Registering command {command}", type.Name);
            serviceCollection.AddSingleton(typeof(IBotCommand), type);
        }
    }
}