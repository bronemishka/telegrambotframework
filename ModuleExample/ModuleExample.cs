﻿using ModuleBase;
using Telegram.Bot;

namespace ModuleExample;

public class ModuleExample : IBotModule
{
    public static ITelegramBotClient? Client { get; set; }
    public Version ModuleVersion { get; } = new (1, 0, 0);
    public string ModuleName => "My Module =)";

    public void LoadModule(ITelegramBotClient client)
    {
        // TODO: Make an example
        Client = client;
    }
}