using ModuleBase.Command;
using Telegram.Bot;

namespace ModuleExample;

public class ExampleCommand : IBotCommand
{
    public ExampleCommand(ITelegramBotClient botClient)
    {
        Action = TestCommand;
        _botClient = botClient;
    }
        
    private readonly ITelegramBotClient _botClient;
    public bool HiddenCommand { get; set; }
    public string Name { get; set; } = "test";
    public string? Description { get; set; } = "Test command";
    public bool FreeAccess { get; set; } = true;
    public Func<CommandParam, Task<bool>> Action { get; set; }

    private async Task<bool> TestCommand(CommandParam param)
    {
        await _botClient.SendMessage(param.ChatId, "Ping");
        await Task.Delay(1000);
        await _botClient.SendMessage(param.ChatId, "Pong");
        return true;
    }
}