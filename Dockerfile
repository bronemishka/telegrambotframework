FROM bitnami/dotnet-sdk:latest

RUN mkdir -p /app
WORKDIR /app
COPY ./BotConsole /app

CMD dotnet run
