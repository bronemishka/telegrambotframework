﻿using Serilog;
using TgBotLib;

namespace ConsoleExample;

public static class Program
{
    private static readonly BotCore BotCore = new();
    public static Task Main() => MainAsync();

    private static async Task StartBotRoutine()
    {
        await BotCore.InitBot();
        
        while (!BotCore.BotCancellationToken.IsCancellationRequested)
        {
            await Task.Delay(TimeSpan.FromSeconds(10), BotCore.BotCancellationToken.Token);
        }
    }

    private static async Task MainAsync()
    {
        Console.CancelKeyPress += async delegate
        {
            await BotCore.StopBotInstance();
            Log.Information("Stopping bot...");
            Environment.Exit(0);
        };
        Log.Logger = new LoggerConfiguration()
            .MinimumLevel.Debug()
            .WriteTo.Console()
            .CreateLogger();
        
        await StartBotRoutine();
    }
}