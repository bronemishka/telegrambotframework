﻿using Telegram.Bot;

namespace ModuleBase
{
    public interface IBotModule
    {
        Version ModuleVersion { get; }
        
        string ModuleName { get; }

        void LoadModule(ITelegramBotClient client);
    }
}