using Telegram.Bot;

namespace ModuleBase.Command;

public class CommandParam
{
    public ITelegramBotClient? BotClient { get; set; }

    public long Caller { get; set; }

    public long ChatId { get; set; }

    public IEnumerable<string>? Args { get; set; }
}