﻿namespace ModuleBase.Command;

public interface IBotCommand
{
    public bool HiddenCommand { get; set; }
    
    public string Name { get; set; }

    public string? Description { get; set; }
    
    public bool FreeAccess { get; set; }

    public Func<CommandParam, Task<bool>> Action { get; set; }
}